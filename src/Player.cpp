
#include "Player.h"

Player::Player(int x, int y, Map *map)
{
    this->x   = x;
    this->y   = y;
    this->map = map;
    this->TopChar = map->GetCharXY(x, y);
}

void Player::Move(int x, int y)
{
    if (map->GetCharXY(this->x + x,this->y + y) != '#')
    {
        map->SetCharXY(this->x, this->y, TopChar);
        this->x += x;
        this->y += y;
        TopChar  = map->GetCharXY(this->x,this->y);
        map->SetCharXY(this->x, this->y, '@');
    }
}

int Player::GetX()
{
    return x;
}

int Player::GetY()
{
    return y;
}


Player::~Player()
{

}
