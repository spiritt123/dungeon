
#include "Map.h"

Map::Map(std::string fileName)
{
    std::string buff;
    std::ifstream fin(fileName);
    if (!fin.is_open()) 
        std::cout << "Файл не может быть открыт!\n"; 
    else
    {
        fin >> row;
        fin >> col;

        map = new char*[row];

        for (int i = 0; i < row; ++i)
        {
            map[i] = new char[col];
            fin >> map[i];
        }


        lightMap = new int*[row];

        for (int i = 0; i < row; ++i)
        {
            lightMap[i] = new int[col];
        }

        lightMap = createLightMap();
        
        fin.close(); 
    }
}

int** Map::createLightMap()
{
    for (int i = 0; i < row; ++i)
    {
        for (int j = 0; j < col; ++j)
        {   
            lightMap[i][j] = 0;
        }
    }

    for (int i = 1; i < row - 1; ++i)
    {
        for (int j = 1; j < col - 1; ++j)
        {
            if (map[i][j] == '!')
            {
                for (int i_v = i - 1; i_v <= i + 1; ++i_v)
                {
                    for (int j_v = j - 1; j_v <= j + 1; ++j_v)
                    {
                        lightMap[i_v][j_v] = 1;
                    }
                }
            }
        }
    }
    return lightMap;
}

char Map::GetCharXY(int x, int y)
{
    return map[y][x];
}

void Map::SetCharXY(int x, int y, char ch)
{
    map[y][x] = ch;
}

bool Map::isPlayerInRect(int x, int y, int i, int j, int r)
{
    if ((j - r <= x) && (j + r >= x))
    {
        if ((i - r <= y) && (i + r >= y))
        {
            return true;
        }
    }
    return false;
}

void Map::printMap(int x, int y)
{
    int R = 4, r = 2;
    for (int i = y - R; i <= y + R; ++i)
    {
        for (int j = x - R; j <= x + R; ++j)
        {
            if ((i >= 0) && (j >= 0) && (i < row) && (j < col))
            {
                if (isPlayerInRect(x, y, i, j, r) || (lightMap[i][j] == 1))
                {
                   std::cout << map[i][j] << ' ';
                }
                else
                {
                    std::cout << "  ";   
                }
            }
            else
            {
                std::cout << "  ";   
            }

        }
        std::cout << std::endl;
    }
}


Map::~Map()
{

}
