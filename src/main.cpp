
#include "unistd.h"

#include "Map.h"
#include "Player.h"
#include "_kbhit.h"


int main(){

    Map *map = new Map("map.txt");
    Player *player = new Player(1, 4, map);
    
    
    while(true)
    {
        //enable_raw_mode();
        system("clear");
        map->printMap(player->GetX(), player->GetY());
        int kay_buttom = _kbhit();
    	
        if (kay_buttom > 0)
        {
            if (kay_buttom == 100) //d
            {
                player->Move(1, 0);
            }
            if (kay_buttom == 115) //s
            {
                player->Move(0, 1);
            }
            if (kay_buttom == 97)  //a
            {
                player->Move(-1, 0);
            }
            if (kay_buttom == 119) //w
            {
                player->Move(0, -1);
            }
            usleep(400000);
        }
        else
        {
            usleep(500000);  
        }
        //disable_raw_mode();
        //tcflush(0, TCIFLUSH);
    }

	return 0;
}

