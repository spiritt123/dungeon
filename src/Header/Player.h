#ifndef PLAYER_H
#define PLAYER_H

#include "Map.h"

class Player
{
public:
    Player(int x, int y, Map *map);
    void Move(int x, int y);
    int  GetX();
    int  GetY();
    ~Player();

private:
    int x;
    int y;
    Map* map;
    char TopChar;

};

#endif//PLAYER_H
