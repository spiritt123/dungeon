#ifndef MAP_H
#define MAP_H

#include <fstream>
#include <iostream>
#include <string>

class Map
{
public:
    Map(std::string fileName);
    int** createLightMap();
    void  printMap(int x, int y);
    char  GetCharXY(int x, int y);
    void  SetCharXY(int x, int y, char ch);
    ~Map();

private:
    bool  isPlayerInRect(int x, int y, int i, int j, int r);

private:
    int  row;
    int  col;
    char **map;
    int  **lightMap;
};



#endif//MAP_H
